﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PriceList.Models
{
    public class Items
    {
        public int ItemsId { get; set; }
        public string ItemName { get; set; }
        public string ItemPrice { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public string NameOfShop { get; set; }
        public string ShopContact { get; set; }
        public string YourContact { get; set; }
    }
}
