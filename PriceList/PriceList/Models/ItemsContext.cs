﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PriceList.Models
{
    public class ItemsContext:DbContext
    {
        public ItemsContext(DbContextOptions<ItemsContext> option) : base(option)
        {
           
        }
        public DbSet<Items> Items { get; set; }


    }
}
